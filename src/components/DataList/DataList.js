import React from 'react';
import './dataList.css';
import SingleListItem from './SingleListItem';
import PropTypes from 'prop-types';

class DataList extends React.Component {
  render(){
    return (
        this.props.data.map( (item) => (
            // Set Unique key.
            <SingleListItem key={item.id} 
                            item={item} 
                            sectionClicked={this.props.sectionClicked} 
                            itemDelete= {this.props.delectedItem} />
        ))
      );
  };
}

// React props types....
DataList.propTypes = {
    data: PropTypes.array.isRequired,
    sectionClicked: PropTypes.func.isRequired,
    delectedItem: PropTypes.func.isRequired
};

export default DataList;
