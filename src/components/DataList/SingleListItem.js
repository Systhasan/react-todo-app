import React from 'react';
import PropTypes from 'prop-types';


class SingleListItem extends React.Component {
 
    divStyled = () => {
        return{
            backgroundColor: this.props.item.status ? '#2E8B57':'#FA8072',
            padding: '10px',
            margin: '10px'
        }
    }
    render(){
        const { id, title, desc } = this.props.item; 

        return (
            <div style={this.divStyled()} >
                <div onClick={this.props.sectionClicked.bind(this, id)}>
                    <p style={{color:'#fff',fontSize:'18px'}}> 
                        Name:  {title} 
                    </p>
                    <p style={{color:'#fff'}}> 
                        Description: {desc} 
                    </p>
                </div>
                <button style={btnStyle} onClick={this.props.itemDelete.bind(this, id)}>X</button>
            </div>
        )
    };
}

// React props types....
SingleListItem.propTypes = {
    item: PropTypes.object.isRequired
}

const btnStyle = {
    background: '#ff0000',
    color: '#fff',
    border:'none',
    padding: '5px 10px',
    borderRadius:'50%',
    cursor: 'pointer'
}
export default SingleListItem;
