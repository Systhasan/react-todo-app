import React, { Component } from 'react'
import './style.css'

export default class addItem extends Component {

    state = {
        title: '',
        desc: ''
    }

    onChangeTitle = (e) => this.setState({title:e.target.value});
    onChangeDesc  = (e) => this.setState({desc:e.target.value});

    onSubmit = (e) => {
        e.preventDefault();
        this.props.addTodo(this.state.title, this.state.desc);
        this.setState({title: ''});
        this.setState({desc: ''});
    }

    render() {
        return (
            <div>
                <form onSubmit={this.onSubmit}>
                    <input  id="name-field" 
                            type="text" 
                            name="item_name" 
                            placeholder="ToDo Title"
                            value={this.state.title}
                            onChange={this.onChangeTitle}/>
                    <input  id="details-field" 
                            type="text" 
                            name="item_desc" 
                            placeholder="ToDo Details"
                            value={this.state.desc}
                            onChange={this.onChangeDesc}/>
                    <input id="submit-btn" type="submit" value="Add Todo"/>
                </form>
            </div>
        )
    }
}
