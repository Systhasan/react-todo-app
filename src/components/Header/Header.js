import React from 'react';
import { Link } from 'react-router-dom';

export default function Header() {
    return (
        <div style={headerStyle}>
            <h1>ToDo List</h1>
            <Link style={menu} to='/'> Home </Link> |
            <Link style={menu} to='/about'> About </Link>
        </div>
    )
}

const headerStyle = {
    margin:'0px',
    background:'#000',
    color: '#fff',
    textAlign:'center',
    padding:'5px'
}

const menu = {
    color: '#fff',
    textDecoration: 'none'
}