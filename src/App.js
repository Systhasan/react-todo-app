import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import Header from './components/Header/Header';
import DataList from './components/DataList/DataList';
import AddItem from './components/addItem/addItem';
import About from './components/pages/about';
import uuid from 'uuid';



class App extends React.Component {

  // Data Source.
  state = {
    dataList: [
      {
        id: uuid.v4(),
        title: 'Milk',
        desc: 'I dont eat tea. ',
        status: false
      },
      {
        id: uuid.v4(),
        title: 'Tea',
        desc: 'Give me some milk.',
        status: false
      },
      {
        id: uuid.v4(),
        title: 'Suger',
        desc: 'Give me some milk.',
        status: false
      }
    ]
  }

  // Change data status.
  sectionClick = (id) => {
    this.setState({ dataList: this.state.dataList.map( data => {
        if(data.id === id){
          data.status = !data.status
        }
        return data; 
      })
    });
  }

  // Delete data item from the data list.
  deleteItem = (id) => {
   this.setState({ dataList: [...this.state.dataList.filter(data => data.id !== id )] });
  };

  // Add new data item to the list.
  addTodo = (title, desc) => {
    const newTodo = {
      id:uuid.v4(),
      title,
      desc,
      status:false
    };
    this.setState({ dataList: [...this.state.dataList, newTodo]});
  }

  render(){
    return (
      <Router>
        <div className="App">
          <Header />
          {/* Load Home page */}
          <Route exact path="/" render={ props => (
            <React.Fragment>
              <header className="App-header">
                <AddItem addTodo={this.addTodo} />
                <DataList data={this.state.dataList} 
                          sectionClicked={this.sectionClick}
                          delectedItem={this.deleteItem}/>
              </header>
            </React.Fragment>
          )} />
          {/* Load Abot page */}
          <Route path="/about" component={About} />
        </div>
      </Router>
    )
  };

}
export default App;
